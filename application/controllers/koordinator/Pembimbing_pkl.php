<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembimbing_pkl extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Pembimbing_pkl_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));        
				$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['title'] = 'pembimbing_pkl';
			$this->get_Meta();
			
			$this->data['_view']='pembimbing_pkl/pembimbing_pkl_list';
			$this->_render_page('layouts/main',$this->data);
		}
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Pembimbing_pkl_model->json();
    }

    public function read($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->Pembimbing_pkl_model->get_by_id($id);
			if ($row) {
				$this->data['kode_bimbingan'] = $this->form_validation->set_value('kode_bimbingan',$row->kode_bimbingan);
				$this->data['id_mahasiswa'] = $this->form_validation->set_value('id_mahasiswa',$row->id_mahasiswa);
				$this->data['id_dosen'] = $this->form_validation->set_value('id_dosen',$row->id_dosen);
				$this->data['semeter'] = $this->form_validation->set_value('semeter',$row->semeter);
				$this->data['judul_laporan'] = $this->form_validation->set_value('judul_laporan',$row->judul_laporan);
	    
				$this->data['title'] = 'pembimbing_pkl';
				$this->get_Meta();
				$this->data['_view'] = 'pembimbing_pkl/pembimbing_pkl_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('pembimbing_pkl'));
			}
		}
    }

    public function create() 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('pembimbing_pkl/create_action');
		    $this->data['kode_bimbingan'] = array(
				'name'			=> 'kode_bimbingan',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_bimbingan'),
				'class'			=> 'form-control',
			);
		    $this->data['id_mahasiswa'] = array(
				'name'			=> 'id_mahasiswa',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('id_mahasiswa'),
				'class'			=> 'form-control',
			);
		    $this->data['id_dosen'] = array(
				'name'			=> 'id_dosen',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('id_dosen'),
				'class'			=> 'form-control',
			);
		    $this->data['semeter'] = array(
				'name'			=> 'semeter',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('semeter'),
				'class'			=> 'form-control',
			);
		    $this->data['judul_laporan'] = array(
				'name'			=> 'judul_laporan',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('judul_laporan'),
				'class'			=> 'form-control',
			);
	
			$this->data['title'] = 'pembimbing_pkl';
			$this->get_Meta();
			$this->data['_view'] = 'pembimbing_pkl/pembimbing_pkl_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_mahasiswa' 			=> $this->input->post('id_mahasiswa',TRUE),
		'id_dosen' 			=> $this->input->post('id_dosen',TRUE),
		'semeter' 			=> $this->input->post('semeter',TRUE),
		'judul_laporan' 			=> $this->input->post('judul_laporan',TRUE),
	    );

            $this->Pembimbing_pkl_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('pembimbing_pkl'));
        }
    }
    
    public function update($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->Pembimbing_pkl_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('pembimbing_pkl/update_action');
			    $this->data['kode_bimbingan'] = array(
					'name'			=> 'kode_bimbingan',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_bimbingan', $row->kode_bimbingan),
					'class'			=> 'form-control',
				);
			    $this->data['id_mahasiswa'] = array(
					'name'			=> 'id_mahasiswa',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('id_mahasiswa', $row->id_mahasiswa),
					'class'			=> 'form-control',
				);
			    $this->data['id_dosen'] = array(
					'name'			=> 'id_dosen',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('id_dosen', $row->id_dosen),
					'class'			=> 'form-control',
				);
			    $this->data['semeter'] = array(
					'name'			=> 'semeter',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('semeter', $row->semeter),
					'class'			=> 'form-control',
				);
			    $this->data['judul_laporan'] = array(
					'name'			=> 'judul_laporan',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('judul_laporan', $row->judul_laporan),
					'class'			=> 'form-control',
				);
	   
				$this->data['title'] = 'pembimbing_pkl';
				$this->get_Meta();
				$this->data['_view'] = 'pembimbing_pkl/pembimbing_pkl_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('pembimbing_pkl'));
			}
		}
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_bimbingan', TRUE));
        } else {
            $data = array(
			'id_mahasiswa' 					=> $this->input->post('id_mahasiswa',TRUE),
			'id_dosen' 					=> $this->input->post('id_dosen',TRUE),
			'semeter' 					=> $this->input->post('semeter',TRUE),
			'judul_laporan' 					=> $this->input->post('judul_laporan',TRUE),
	    );

            $this->Pembimbing_pkl_model->update($this->input->post('kode_bimbingan', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('pembimbing_pkl'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pembimbing_pkl_model->get_by_id($id);

        if ($row) {
            $this->Pembimbing_pkl_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('pembimbing_pkl'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('pembimbing_pkl'));
        }
    }
	
	public function get_Meta(){
		
		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {			
			$this->data['name_web'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
    public function _rules() 
    {
	$this->form_validation->set_rules('id_mahasiswa', 'id mahasiswa', 'trim|required');
	$this->form_validation->set_rules('id_dosen', 'id dosen', 'trim|required');
	$this->form_validation->set_rules('semeter', 'semeter', 'trim|required');
	$this->form_validation->set_rules('judul_laporan', 'judul laporan', 'trim|required');

	$this->form_validation->set_rules('kode_bimbingan', 'kode_bimbingan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pembimbing_pkl.xls";
        $judul = "pembimbing_pkl";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Mahasiswa");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Dosen");
	xlsWriteLabel($tablehead, $kolomhead++, "Semeter");
	xlsWriteLabel($tablehead, $kolomhead++, "Judul Laporan");

	foreach ($this->Pembimbing_pkl_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_mahasiswa);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_dosen);
	    xlsWriteLabel($tablebody, $kolombody++, $data->semeter);
	    xlsWriteLabel($tablebody, $kolombody++, $data->judul_laporan);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdf()
    {
        $data = array(
            'pembimbing_pkl_data' => $this->Pembimbing_pkl_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('pembimbing_pkl/pembimbing_pkl_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('pembimbing_pkl.pdf', 'D'); 
    }

}

/* End of file Pembimbing_pkl.php */
/* Location: ./application/controllers/Pembimbing_pkl.php */
