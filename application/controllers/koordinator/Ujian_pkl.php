<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ujian_pkl extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Ujian_pkl_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));        
				$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['title'] = 'ujian_pkl';
			$this->get_Meta();
			
			$this->data['_view']='ujian_pkl/ujian_pkl_list';
			$this->_render_page('layouts/main',$this->data);
		}
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Ujian_pkl_model->json();
    }

    public function read($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->Ujian_pkl_model->get_by_id($id);
			if ($row) {
				$this->data['kode_ujian'] = $this->form_validation->set_value('kode_ujian',$row->kode_ujian);
				$this->data['id_mahasiswa'] = $this->form_validation->set_value('id_mahasiswa',$row->id_mahasiswa);
				$this->data['id_dosen_penguji'] = $this->form_validation->set_value('id_dosen_penguji',$row->id_dosen_penguji);
				$this->data['hari_tanggal'] = $this->form_validation->set_value('hari_tanggal',$row->hari_tanggal);
				$this->data['waktu'] = $this->form_validation->set_value('waktu',$row->waktu);
				$this->data['tempat_pelaksanaan'] = $this->form_validation->set_value('tempat_pelaksanaan',$row->tempat_pelaksanaan);
	    
				$this->data['title'] = 'ujian_pkl';
				$this->get_Meta();
				$this->data['_view'] = 'ujian_pkl/ujian_pkl_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('ujian_pkl'));
			}
		}
    }

    public function create() 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('ujian_pkl/create_action');
		    $this->data['kode_ujian'] = array(
				'name'			=> 'kode_ujian',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_ujian'),
				'class'			=> 'form-control',
			);
		    $this->data['id_mahasiswa'] = array(
				'name'			=> 'id_mahasiswa',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('id_mahasiswa'),
				'class'			=> 'form-control',
			);
		    $this->data['id_dosen_penguji'] = array(
				'name'			=> 'id_dosen_penguji',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('id_dosen_penguji'),
				'class'			=> 'form-control',
			);
		    $this->data['hari_tanggal'] = array(
				'name'			=> 'hari_tanggal',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('hari_tanggal'),
				'class'			=> 'form-control',
			);
		    $this->data['waktu'] = array(
				'name'			=> 'waktu',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('waktu'),
				'class'			=> 'form-control',
			);
		    $this->data['tempat_pelaksanaan'] = array(
				'name'			=> 'tempat_pelaksanaan',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('tempat_pelaksanaan'),
				'class'			=> 'form-control',
			);
	
			$this->data['title'] = 'ujian_pkl';
			$this->get_Meta();
			$this->data['_view'] = 'ujian_pkl/ujian_pkl_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_mahasiswa' 			=> $this->input->post('id_mahasiswa',TRUE),
		'id_dosen_penguji' 			=> $this->input->post('id_dosen_penguji',TRUE),
		'hari_tanggal' 			=> $this->input->post('hari_tanggal',TRUE),
		'waktu' 			=> $this->input->post('waktu',TRUE),
		'tempat_pelaksanaan' 			=> $this->input->post('tempat_pelaksanaan',TRUE),
	    );

            $this->Ujian_pkl_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('ujian_pkl'));
        }
    }
    
    public function update($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->Ujian_pkl_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('ujian_pkl/update_action');
			    $this->data['kode_ujian'] = array(
					'name'			=> 'kode_ujian',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_ujian', $row->kode_ujian),
					'class'			=> 'form-control',
				);
			    $this->data['id_mahasiswa'] = array(
					'name'			=> 'id_mahasiswa',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('id_mahasiswa', $row->id_mahasiswa),
					'class'			=> 'form-control',
				);
			    $this->data['id_dosen_penguji'] = array(
					'name'			=> 'id_dosen_penguji',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('id_dosen_penguji', $row->id_dosen_penguji),
					'class'			=> 'form-control',
				);
			    $this->data['hari_tanggal'] = array(
					'name'			=> 'hari_tanggal',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('hari_tanggal', $row->hari_tanggal),
					'class'			=> 'form-control',
				);
			    $this->data['waktu'] = array(
					'name'			=> 'waktu',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('waktu', $row->waktu),
					'class'			=> 'form-control',
				);
			    $this->data['tempat_pelaksanaan'] = array(
					'name'			=> 'tempat_pelaksanaan',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('tempat_pelaksanaan', $row->tempat_pelaksanaan),
					'class'			=> 'form-control',
				);
	   
				$this->data['title'] = 'ujian_pkl';
				$this->get_Meta();
				$this->data['_view'] = 'ujian_pkl/ujian_pkl_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('ujian_pkl'));
			}
		}
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_ujian', TRUE));
        } else {
            $data = array(
			'id_mahasiswa' 					=> $this->input->post('id_mahasiswa',TRUE),
			'id_dosen_penguji' 					=> $this->input->post('id_dosen_penguji',TRUE),
			'hari_tanggal' 					=> $this->input->post('hari_tanggal',TRUE),
			'waktu' 					=> $this->input->post('waktu',TRUE),
			'tempat_pelaksanaan' 					=> $this->input->post('tempat_pelaksanaan',TRUE),
	    );

            $this->Ujian_pkl_model->update($this->input->post('kode_ujian', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('ujian_pkl'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Ujian_pkl_model->get_by_id($id);

        if ($row) {
            $this->Ujian_pkl_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('ujian_pkl'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('ujian_pkl'));
        }
    }
	
	public function get_Meta(){
		
		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {			
			$this->data['name_web'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
    public function _rules() 
    {
	$this->form_validation->set_rules('id_mahasiswa', 'id mahasiswa', 'trim|required');
	$this->form_validation->set_rules('id_dosen_penguji', 'id dosen penguji', 'trim|required');
	$this->form_validation->set_rules('hari_tanggal', 'hari tanggal', 'trim|required');
	$this->form_validation->set_rules('waktu', 'waktu', 'trim|required');
	$this->form_validation->set_rules('tempat_pelaksanaan', 'tempat pelaksanaan', 'trim|required');

	$this->form_validation->set_rules('kode_ujian', 'kode_ujian', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "ujian_pkl.xls";
        $judul = "ujian_pkl";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Mahasiswa");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Dosen Penguji");
	xlsWriteLabel($tablehead, $kolomhead++, "Hari Tanggal");
	xlsWriteLabel($tablehead, $kolomhead++, "Waktu");
	xlsWriteLabel($tablehead, $kolomhead++, "Tempat Pelaksanaan");

	foreach ($this->Ujian_pkl_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_mahasiswa);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_dosen_penguji);
	    xlsWriteLabel($tablebody, $kolombody++, $data->hari_tanggal);
	    xlsWriteLabel($tablebody, $kolombody++, $data->waktu);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tempat_pelaksanaan);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdf()
    {
        $data = array(
            'ujian_pkl_data' => $this->Ujian_pkl_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('ujian_pkl/ujian_pkl_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('ujian_pkl.pdf', 'D'); 
    }

}

/* End of file Ujian_pkl.php */
/* Location: ./application/controllers/Ujian_pkl.php */
