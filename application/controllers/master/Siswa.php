<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Mahasiswa_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));
				$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$this->data['title'] = 'Data Mahasiswa';
			$this->get_Meta();

			$this->data['_view']='master/mahasiswa/m_mahasiswa_list';
			$this->_render_page('layouts/main',$this->data);
		}
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->Mahasiswa_model->json();
    }

    public function read($id)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$row = $this->Mahasiswa_model->get_by_id($id);
			if ($row) {
				$this->data['id_mahasiswa'] = $this->form_validation->set_value('id_mahasiswa',$row->id_mahasiswa);
				$this->data['kode_prodi'] = $this->form_validation->set_value('kode_prodi',$row->kode_prodi);
				$this->data['nama_mahasiswa'] = $this->form_validation->set_value('nama_mahasiswa',$row->nama_mahasiswa);
				$this->data['jenis_kelamin'] = $this->form_validation->set_value('jenis_kelamin',$row->jenis_kelamin);
				$this->data['alamat'] = $this->form_validation->set_value('alamat',$row->alamat);
				$this->data['no_telp'] = $this->form_validation->set_value('no_telp',$row->no_telp);
				$this->data['email'] = $this->form_validation->set_value('email',$row->email);

				$this->data['title'] = 'Data Mahasiswa';
				$this->get_Meta();
				$this->data['_view'] = 'master/mahasiswa/m_mahasiswa_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('master/siswa'));
			}
		}
    }

    public function create()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('master/siswa/create_action');
		    $this->data['id_mahasiswa'] = array(
				'name'			=> 'id_mahasiswa',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('id_mahasiswa'),
				'class'			=> 'form-control',
			);
		    $this->data['kode_prodi'] = array(
				'name'			=> 'kode_prodi',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_prodi'),
				'class'			=> 'form-control',
			);
		    $this->data['nama_mahasiswa'] = array(
				'name'			=> 'nama_mahasiswa',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nama_mahasiswa'),
				'class'			=> 'form-control',
			);
		    $this->data['jenis_kelamin'] = array(
				'name'			=> 'jenis_kelamin',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('jenis_kelamin'),
				'class'			=> 'form-control',
			);
		    $this->data['alamat'] = array(
				'name'			=> 'alamat',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('alamat'),
				'class'			=> 'form-control',
			);
		    $this->data['no_telp'] = array(
				'name'			=> 'no_telp',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('no_telp'),
				'class'			=> 'form-control',
			);
		    $this->data['email'] = array(
				'name'			=> 'email',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('email'),
				'class'			=> 'form-control',
			);

			$this->data['title'] = 'Data Mahasiswa';
			$this->get_Meta();
			$this->data['_view'] = 'master/mahasiswa/m_mahasiswa_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kode_prodi' 			=> $this->input->post('kode_prodi',TRUE),
		'nama_mahasiswa' 			=> $this->input->post('nama_mahasiswa',TRUE),
		'jenis_kelamin' 			=> $this->input->post('jenis_kelamin',TRUE),
		'alamat' 			=> $this->input->post('alamat',TRUE),
		'no_telp' 			=> $this->input->post('no_telp',TRUE),
		'email' 			=> $this->input->post('email',TRUE),
	    );

            $this->Mahasiswa_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('master/siswa'));
        }
    }

    public function update($id)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$row = $this->Mahasiswa_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('master/siswa/update_action');
			    $this->data['id_mahasiswa'] = array(
					'name'			=> 'id_mahasiswa',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('id_mahasiswa', $row->id_mahasiswa),
					'class'			=> 'form-control',
				);
			    $this->data['kode_prodi'] = array(
					'name'			=> 'kode_prodi',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_prodi', $row->kode_prodi),
					'class'			=> 'form-control',
				);
			    $this->data['nama_mahasiswa'] = array(
					'name'			=> 'nama_mahasiswa',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('nama_mahasiswa', $row->nama_mahasiswa),
					'class'			=> 'form-control',
				);
			    $this->data['jenis_kelamin'] = array(
					'name'			=> 'jenis_kelamin',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('jenis_kelamin', $row->jenis_kelamin),
					'class'			=> 'form-control',
				);
			    $this->data['alamat'] = array(
					'name'			=> 'alamat',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('alamat', $row->alamat),
					'class'			=> 'form-control',
				);
			    $this->data['no_telp'] = array(
					'name'			=> 'no_telp',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('no_telp', $row->no_telp),
					'class'			=> 'form-control',
				);
			    $this->data['email'] = array(
					'name'			=> 'email',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('email', $row->email),
					'class'			=> 'form-control',
				);

				$this->data['title'] = 'Data Mahasiswa';
				$this->get_Meta();
				$this->data['_view'] = 'master/mahasiswa/m_mahasiswa_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('master/siswa'));
			}
		}
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_mahasiswa', TRUE));
        } else {
            $data = array(
			'kode_prodi' 					=> $this->input->post('kode_prodi',TRUE),
			'nama_mahasiswa' 					=> $this->input->post('nama_mahasiswa',TRUE),
			'jenis_kelamin' 					=> $this->input->post('jenis_kelamin',TRUE),
			'alamat' 					=> $this->input->post('alamat',TRUE),
			'no_telp' 					=> $this->input->post('no_telp',TRUE),
			'email' 					=> $this->input->post('email',TRUE),
	    );

            $this->Mahasiswa_model->update($this->input->post('id_mahasiswa', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('master/siswa'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mahasiswa_model->get_by_id($id);

        if ($row) {
            $this->Mahasiswa_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('master/siswa'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('master/siswa'));
        }
    }

	public function get_Meta(){

		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {
			$this->data['name_web'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}

	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

    public function _rules()
    {
	$this->form_validation->set_rules('kode_prodi', 'kode prodi', 'trim|required');
	$this->form_validation->set_rules('nama_mahasiswa', 'nama mahasiswa', 'trim|required');
	$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');

	$this->form_validation->set_rules('id_mahasiswa', 'id_mahasiswa', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "m_mahasiswa.xls";
        $judul = "m_mahasiswa";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode Prodi");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Mahasiswa");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Kelamin");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
	xlsWriteLabel($tablehead, $kolomhead++, "No Telp");
	xlsWriteLabel($tablehead, $kolomhead++, "Email");

	foreach ($this->Mahasiswa_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kode_prodi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_mahasiswa);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis_kelamin);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_telp);
	    xlsWriteLabel($tablebody, $kolombody++, $data->email);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdf()
    {
        $data = array(
            'm_mahasiswa_data' => $this->Mahasiswa_model->get_all(),
            'start' => 0
        );

        ini_set('memory_limit', '32M');
        $html = $this->load->view('m_mahasiswa/m_mahasiswa_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('m_mahasiswa.pdf', 'D');
    }

}

/* End of file M_mahasiswa.php */
/* Location: ./application/controllers/M_mahasiswa.php */
