<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Program_studi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Program_studi_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));
				$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$this->data['title'] = 'program_studi';
			$this->get_Meta();

			$this->data['_view'] = 'master/program_studi/program_studi_list';
			$this->_render_page('layouts/main',$this->data);
		}
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->Program_studi_model->json();
    }

    public function read($id)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$row = $this->Program_studi_model->get_by_id($id);
			if ($row) {
				$this->data['kode_prodi'] = $this->form_validation->set_value('kode_prodi',$row->kode_prodi);
				$this->data['nama_prodi'] = $this->form_validation->set_value('nama_prodi',$row->nama_prodi);
				$this->data['jenjang'] = $this->form_validation->set_value('jenjang',$row->jenjang);

				$this->data['title'] = 'program_studi';
				$this->get_Meta();
				$this->data['_view'] = 'master/program_studi/program_studi_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('program_studi'));
			}
		}
    }

    public function create()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('master/program_studi/create_action');
		    $this->data['kode_prodi'] = array(
				'name'			=> 'kode_prodi',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_prodi'),
				'class'			=> 'form-control',
			);
		    $this->data['nama_prodi'] = array(
				'name'			=> 'nama_prodi',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nama_prodi'),
				'class'			=> 'form-control',
			);
		    $this->data['jenjang'] = array(
				'name'			=> 'jenjang',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('jenjang'),
				'class'			=> 'form-control',
			);

			$this->data['title'] = 'program_studi';
			$this->get_Meta();
			$this->data['_view'] = 'master/program_studi/program_studi_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_prodi' 			=> $this->input->post('nama_prodi',TRUE),
		'jenjang' 			=> $this->input->post('jenjang',TRUE),
	    );

            $this->Program_studi_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('program_studi'));
        }
    }

    public function update($id)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$row = $this->Program_studi_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('master/program_studi/update_action');
			    $this->data['kode_prodi'] = array(
					'name'			=> 'kode_prodi',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_prodi', $row->kode_prodi),
					'class'			=> 'form-control',
				);
			    $this->data['nama_prodi'] = array(
					'name'			=> 'nama_prodi',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('nama_prodi', $row->nama_prodi),
					'class'			=> 'form-control',
				);
			    $this->data['jenjang'] = array(
					'name'			=> 'jenjang',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('jenjang', $row->jenjang),
					'class'			=> 'form-control',
				);

				$this->data['title'] = 'Program Studi';
				$this->get_Meta();
				$this->data['_view'] = 'master/program_studi/program_studi_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('program_studi'));
			}
		}
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_prodi', TRUE));
        } else {
            $data = array(
			'nama_prodi' 					=> $this->input->post('nama_prodi',TRUE),
			'jenjang' 					=> $this->input->post('jenjang',TRUE),
	    );

            $this->Program_studi_model->update($this->input->post('kode_prodi', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('program_studi'));
        }
    }

    public function delete($id)
    {
        $row = $this->Program_studi_model->get_by_id($id);

        if ($row) {
            $this->Program_studi_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('program_studi'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('program_studi'));
        }
    }

	public function get_Meta(){

		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {
			$this->data['name_web'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}

	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

    public function _rules()
    {
	$this->form_validation->set_rules('nama_prodi', 'nama prodi', 'trim|required');
	$this->form_validation->set_rules('jenjang', 'jenjang', 'trim|required');

	$this->form_validation->set_rules('kode_prodi', 'kode_prodi', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "program_studi.xls";
        $judul = "program_studi";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Prodi");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenjang");

	foreach ($this->Program_studi_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_prodi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenjang);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdf()
    {
        $data = array(
            'program_studi_data' => $this->Program_studi_model->get_all(),
            'start' => 0
        );

        ini_set('memory_limit', '32M');
        $html = $this->load->view('master/program_studi/program_studi_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('program_studi.pdf', 'D');
    }

}

/* End of file program_studi.php */
/* Location: ./application/controllers/program_studi.php */
