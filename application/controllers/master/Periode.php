<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Periode extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Periode_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));
				$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$this->data['title'] = 'Periode';
			$this->get_Meta();

			$this->data['_view']='master/periode/periode_list';
			$this->_render_page('layouts/main',$this->data);
		}
    }

    public function json() {
        header('Content-Type: application/json');
        echo $this->Periode_model->json();
    }

    public function read($id)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$row = $this->Periode_model->get_by_id($id);
			if ($row) {
				$this->data['kode_periode'] = $this->form_validation->set_value('kode_periode',$row->kode_periode);
				$this->data['nama_periode'] = $this->form_validation->set_value('nama_periode',$row->nama_periode);
				$this->data['tahun_akademik'] = $this->form_validation->set_value('tahun_akademik',$row->tahun_akademik);

				$this->data['title'] = 'Periode';
				$this->get_Meta();
				$this->data['_view'] = 'master/periode/periode_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('master/periode'));
			}
		}
    }

    public function create()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('master/periode/create_action');
		    $this->data['kode_periode'] = array(
				'name'			=> 'kode_periode',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_periode'),
				'class'			=> 'form-control',
			);
		    $this->data['nama_periode'] = array(
				'name'			=> 'nama_periode',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nama_periode'),
				'class'			=> 'form-control',
			);
		    $this->data['tahun_akademik'] = array(
				'name'			=> 'tahun_akademik',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('tahun_akademik'),
				'class'			=> 'form-control',
			);

			$this->data['title'] = 'Periode';
			$this->get_Meta();
			$this->data['_view'] = 'master/periode/periode_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_periode' 			=> $this->input->post('nama_periode',TRUE),
		'tahun_akademik' 			=> $this->input->post('tahun_akademik',TRUE),
	    );

            $this->Periode_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('master/periode'));
        }
    }

    public function update($id)
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['usr'] = $this->ion_auth->user()->row();

			$row = $this->Periode_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('master/periode/update_action');
			    $this->data['kode_periode'] = array(
					'name'			=> 'kode_periode',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_periode', $row->kode_periode),
					'class'			=> 'form-control',
				);
			    $this->data['nama_periode'] = array(
					'name'			=> 'nama_periode',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('nama_periode', $row->nama_periode),
					'class'			=> 'form-control',
				);
			    $this->data['tahun_akademik'] = array(
					'name'			=> 'tahun_akademik',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('tahun_akademik', $row->tahun_akademik),
					'class'			=> 'form-control',
				);

				$this->data['title'] = 'Periode';
				$this->get_Meta();
				$this->data['_view'] = 'master/periode/periode_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('master/periode'));
			}
		}
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_periode', TRUE));
        } else {
            $data = array(
			'nama_periode' 					=> $this->input->post('nama_periode',TRUE),
			'tahun_akademik' 					=> $this->input->post('tahun_akademik',TRUE),
	    );

            $this->Periode_model->update($this->input->post('kode_periode', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('master/periode'));
        }
    }

    public function delete($id)
    {
        $row = $this->Periode_model->get_by_id($id);

        if ($row) {
            $this->Periode_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('master/periode'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('master/periode'));
        }
    }

	public function get_Meta(){

		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {
			$this->data['name_web'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}

	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

    public function _rules()
    {
	$this->form_validation->set_rules('nama_periode', 'nama periode', 'trim|required');
	$this->form_validation->set_rules('tahun_akademik', 'tahun akademik', 'trim|required');

	$this->form_validation->set_rules('kode_periode', 'kode_periode', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "m_periode.xls";
        $judul = "m_periode";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Periode");
	xlsWriteLabel($tablehead, $kolomhead++, "Tahun Akademik");

	foreach ($this->Periode_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_periode);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tahun_akademik);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdf()
    {
        $data = array(
            'm_periode_data' => $this->Periode_model->get_all(),
            'start' => 0
        );

        ini_set('memory_limit', '32M');
        $html = $this->load->view('master/periode/periode_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('periode.pdf', 'D');
    }

}

/* End of file M_periode.php */
/* Location: ./application/controllers/M_periode.php */
