<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_master_tables extends CI_Migration {
	private $tables;

	public function __construct() {
		parent::__construct();
		$this->load->dbforge();
	}

	public function up() {
		$this->create_table_menu();
		$this->create_table_identitas_web();
		$this->create_table_m_dosen();
		$this->create_table_m_program_studi();
		$this->create_table_m_periode();
		$this->create_table_m_mahasiswa();
		$this->generate_data();
	}

	private function create_table_menu()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'parent_menu' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE,
			),
			'nama_menu' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
			),
			'controller_link' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
			),
			'icon' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
			),
			'urut_menu' => array(
				'type' => 'INT',
				'constraint' => '11',
			),
			'menu_grup_user' => array(
				'type' => 'VARCHAR',
				'constraint' => '30',
			),
			'is_active' => array(
				'type' => 'INT',
				'constraint' => '11',
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("id", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table menu
		$this->dbforge->create_table("menu", TRUE, $attributes);
	}
	private function create_table_identitas_web()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'id_identitas' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'nama_web' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
			'meta_deskripsi' => array(
				'type' => 'TEXT',
			),
			'meta_keyword' => array(
				'type' => 'TEXT',
			),
			'copyright' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'logo' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("id_identitas", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table identitas_web
		$this->dbforge->create_table("identitas_web", TRUE, $attributes);
	}
	private function create_table_m_dosen()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'id_dosen' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'kode_prodi' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'nama_dosen' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE,
			),
			'nip' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => TRUE,
			),
			'pangkat' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => TRUE,
			),
			'golongan' => array(
				'type' => 'VARCHAR',
				'constraint' => '5',
				'null' => TRUE,
			),
			'status_dosen' => array(
				'type' => 'VARCHAR',
				'constraint' => '15',
				'null' => TRUE,
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("id_dosen", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table m_dosen
		$this->dbforge->create_table("m_dosen", TRUE, $attributes);
	}
	private function create_table_m_program_studi()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'kode_prodi' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'nama_prodi' => array(
				'type' => 'VARCHAR',
				'constraint' => '30',
				'null' => TRUE,
			),
			'jenjang' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("kode_prodi", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table m_program_studi
		$this->dbforge->create_table("m_program_studi", TRUE, $attributes);
	}
	private function create_table_m_periode()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'kode_periode' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'nama_periode' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'tahun_akademik' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("kode_periode", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table m_periode
		$this->dbforge->create_table("m_periode", TRUE, $attributes);
	}
	private function create_table_m_mahasiswa()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'id_mahasiswa' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'kode_prodi' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'nama_mahasiswa' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE,
			),
			'jenis_kelamin' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'alamat' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE,
			),
			'no_telp' => array(
				'type' => 'VARCHAR',
				'constraint' => '15',
				'null' => TRUE,
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
				'null' => TRUE,
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("id_mahasiswa", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table m_mahasiswa
		$this->dbforge->create_table("m_mahasiswa", TRUE, $attributes);

	}

	private function generate_data()
	{
		$dataMenu = [
			[
				"id" => "1",
				"parent_menu" => "0",
				"nama_menu" => "Master",
				"controller_link" => "#",
				"icon" => "fa-list-alt",
				"slug" => "a",
				"urut_menu" => "1",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "2",
				"parent_menu" => "1",
				"nama_menu" => "Data Periode",
				"controller_link" => "master/periode",
				"icon" => "fa-calendar",
				"slug" => "periode",
				"urut_menu" => "1",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "3",
				"parent_menu" => "1",
				"nama_menu" => "Data Dosen",
				"controller_link" => "master/dosen",
				"icon" => "fa-user-secret",
				"slug" => "dosen",
				"urut_menu" => "2",
				"menu_grup_user" => "2",
				"is_active" => "1"
			],
			[
				"id" => "4",
				"parent_menu" => "1",
				"nama_menu" => "Data Mahasiswa",
				"controller_link" => "master/siswa",
				"icon" => "fa-users",
				"slug" => "siswa",
				"urut_menu" => "3",
				"menu_grup_user" => "2",
				"is_active" => "1"
			],
			[
				"id" => "5",
				"parent_menu" => "1",
				"nama_menu" => "Data Program Sudi",
				"controller_link" => "master/program_studi",
				"icon" => "fa-graduation-cap",
				"slug" => "prodi",
				"urut_menu" => "4",
				"menu_grup_user" => "3",
				"is_active" => "1"
			],
			[
				"id" => "6",
				"parent_menu" => "0",
				"nama_menu" => "PKL",
				"controller_link" => "#",
				"icon" => "fa-address-book-o",
				"slug" => "pkl",
				"urut_menu" => "2",
				"menu_grup_user" => "3",
				"is_active" => "1"
			],
			[
				"id" => "7",
				"parent_menu" => "6",
				"nama_menu" => "Data Pengajuan PKL",
				"controller_link" => "pkl/pengajuan",
				"icon" => "fa-address-book",
				"slug" => "pengajuan-pkl",
				"urut_menu" => "2",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "8",
				"parent_menu" => "6",
				"nama_menu" => "Data Pembimbing PKL",
				"controller_link" => "pkl/pembimbing",
				"icon" => "fa-id-card",
				"slug" => "pembimbing-pkl",
				"urut_menu" => "1",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "9",
				"parent_menu" => "6",
				"nama_menu" => "Data Ujian PKL",
				"controller_link" => "pkl/ujian",
				"icon" => "fa-tasks",
				"slug" => "ujian-pkl",
				"urut_menu" => "3",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "10",
				"parent_menu" => "6",
				"nama_menu" => "Data Nilai PKL",
				"controller_link" => "pkl/nilai",
				"icon" => "fa-file-text",
				"slug" => "nilai-pkl",
				"urut_menu" => "4",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "11",
				"parent_menu" => "0",
				"nama_menu" => "Laporan",
				"controller_link" => "#",
				"icon" => "fa-newspaper-o",
				"slug" => "laporan",
				"urut_menu" => "3",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "12",
				"parent_menu" => "11",
				"nama_menu" => "Laporan Pengajuan PKL",
				"controller_link" => "laporan/pengajuan-pkl",
				"icon" => "fa-newspaper-o",
				"slug" => "lap-pengajuan-pkl",
				"urut_menu" => "1",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "13",
				"parent_menu" => "11",
				"nama_menu" => "Laporan Pembimbing Penguji",
				"controller_link" => "laporan/pembimbing",
				"icon" => "fa-newspaper-o",
				"slug" => "lap-pembimbing-penguji",
				"urut_menu" => "2",
				"menu_grup_user" => "1",
				"is_active" => "1"
			],
			[
				"id" => "14",
				"parent_menu" => "11",
				"nama_menu" => "Laporan Nilai PKL",
				"controller_link" => "laporan/nilai_pkl",
				"icon" => "fa-newspaper-o",
				"slug" => "lap-nillai-pkl",
				"urut_menu" => "3",
				"menu_grup_user" => "1",
				"is_active" => "1"
			]
		];
		$dataIdentitasWeb = [
			[
				"id_identitas" => "1",
				"nama_web" => "SIMPLA",
				"meta_deskripsi" => "asss",
				"meta_keyword" => "asdfss",
				"copyright" => "dx",
				"logo" => "./uploads/1b8631d07e2d37a436894d16eb365f7c.png"
			]
		];

		$this->db->insert_batch('menu', $dataMenu);
		$this->db->insert_batch('identitas_web', $dataIdentitasWeb);
	}

	public function down() {
		// Drop table m_dosen
		$this->dbforge->drop_table("menu", TRUE);
		$this->dbforge->drop_table("identitas_web", TRUE);
		$this->dbforge->drop_table("m_dosen", TRUE);
		$this->dbforge->drop_table("m_program_studi", TRUE);
		$this->dbforge->drop_table("m_periode", TRUE);
		$this->dbforge->drop_table("m_mahasiswa", TRUE);
	}
}
