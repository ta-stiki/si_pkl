<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_data_group extends CI_Migration {
	private $tables;

	public function __construct() {
		parent::__construct();
		$this->load->dbforge();
	}

	public function up() {

		$names = array(1,2,3,4);
		$this->db->where_in('id', $names);
		$this->db->delete('groups');

		$dataUserGroup =  [
			[
				"id" => "1",
				"user_id" => "1",
				"group_id" => "1"
			],
			[
				"id" => "2",
				"user_id" => "1",
				"group_id" => "2"
			],
			[
				"id" => "3",
				"user_id" => "1",
				"group_id" => "3"
			],
			[
				"id" => "4",
				"user_id" => "1",
				"group_id" => "4"
			]
		];

		$dataGroup = [
			[
				"id" => "1",
				"name" => "admin",
				"description" => "Administrator"
			],
			[
				"id" => "2",
				"name" => "koordinator_pkl",
				"description" => "Koordinator PKL"
			],
			[
				"id" => "3",
				"name" => "prodi",
				"description" => "Staff Prodi"
			],
			[
				"id" => "4",
				"name" => "siswa",
				"description" => "Siswa"
			]
		];
		$this->db->insert_batch('groups', $dataGroup);
		$this->db->truncate('users_groups');
		$this->db->insert_batch('users_groups', $dataUserGroup);
	}

	public function down() {
		$names = array(1,2,3,4);
		$this->db->where_in('id', $names);
		$this->db->delete('groups');
	}
}
