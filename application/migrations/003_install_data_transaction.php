<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install_data_transaction extends CI_Migration {
	private $tables;

	public function __construct() {
		parent::__construct();
		$this->load->dbforge();
	}

	public function up() {
		$this->create_table_ujian_pkl();
		$this->create_table_pengajuan_pkl();
		$this->create_table_pembimbing_pkl();
		$this->create_table_nilai_pkl();
	}

	private function create_table_ujian_pkl()
	{
		// Add Fields.
	$this->dbforge->add_field(array(
		'kode_ujian' => array(
			'type' => 'VARCHAR',
			'constraint' => '10',
		),
		'id_mahasiswa' => array(
			'type' => 'VARCHAR',
			'constraint' => '10',
			'null' => TRUE,
		),
		'id_dosen_penguji' => array(
			'type' => 'VARCHAR',
			'constraint' => '10',
			'null' => TRUE,
		),
		'hari_tanggal' => array(
			'type' => 'DATE',
			'null' => TRUE,
		),
		'waktu' => array(
			'type' => 'TIME',
			'null' => TRUE,
		),
		'tempat_pelaksanaan' => array(
			'type' => 'VARCHAR',
			'constraint' => '25',
			'null' => TRUE,
		),
	));

        // Add Primary Key.
        $this->dbforge->add_key("kode_ujian", TRUE);

        // Table attributes.

        $attributes = array(
			'ENGINE' => 'InnoDB',
		);

        // Create Table ujian_pkl
        $this->dbforge->create_table("ujian_pkl", TRUE, $attributes);
	}
	private function create_table_pengajuan_pkl()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'kode_pkl' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'id_mahasiswa' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'semester' => array(
				'type' => 'VARCHAR',
				'constraint' => '15',
				'null' => TRUE,
			),
			'tujuan_tempat_pkl' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE,
			),
			'alamat_tempat_pkl' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE,
			),
			'no_telp_tempat_pkl' => array(
				'type' => 'VARCHAR',
				'constraint' => '15',
				'null' => TRUE,
			),
			'lama_pkl' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'periode_awal' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'periode_akhir' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("kode_pkl", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table pengajuan_pkl
		$this->dbforge->create_table("pengajuan_pkl", TRUE, $attributes);
	}
	private function create_table_pembimbing_pkl()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'kode_bimbingan' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'id_mahasiswa' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'id_dosen' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'semeter' => array(
				'type' => 'VARCHAR',
				'constraint' => '15',
				'null' => TRUE,
			),
			'judul_laporan' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("kode_bimbingan", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table pembimbing_pkl
		$this->dbforge->create_table("pembimbing_pkl", TRUE, $attributes);
	}
	private function create_table_nilai_pkl()
	{
		// Add Fields.
		$this->dbforge->add_field(array(
			'kode_nilai' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'kode_ujian' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'null' => TRUE,
			),
			'nilai_relatif' => array(
				'type' => 'VARCHAR',
				'constraint' => '2',
				'null' => TRUE,
			),
			'nilai_absolut' => array(
				'type' => 'VARCHAR',
				'constraint' => '5',
				'null' => TRUE,
			),
		));

		// Add Primary Key.
		$this->dbforge->add_key("kode_nilai", TRUE);

		// Table attributes.

		$attributes = array(
			'ENGINE' => 'InnoDB',
		);

		// Create Table nilai_pkl
		$this->dbforge->create_table("nilai_pkl", TRUE, $attributes);
	}

	public function down() {
		$this->dbforge->drop_table("ujian_pkl", TRUE);
		$this->dbforge->drop_table("pengajuan_pkl", TRUE);
		$this->dbforge->drop_table("pembimbing_pkl", TRUE);
		$this->dbforge->drop_table("nilai_pkl", TRUE);
	}
}
