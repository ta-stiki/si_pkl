<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dosen_model extends CI_Model
{

    public $table = 'm_dosen';
    public $id = 'id_dosen';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id_dosen,kode_prodi,nama_dosen,nip,pangkat,golongan,status_dosen');
        $this->datatables->from('m_dosen');
        //add this line for join
        //$this->datatables->join('table2', 'm_dosen.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('master/dosen/read/$1'),'<i class = "fa fa-eye"></i>', array('class'=>'btn btn-flat btn-info'))." ".anchor(site_url('master/dosen/update/$1'),'<i class = "fa fa-edit"></i>', array('class'=>'btn btn-flat btn-warning'))." ".anchor(site_url('master/dosen/delete/$1'),'<i class = "fa fa-trash"></i>', array('class'=>'btn btn-flat btn-danger'),'onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_dosen');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_dosen', $q);
	$this->db->or_like('kode_prodi', $q);
	$this->db->or_like('nama_dosen', $q);
	$this->db->or_like('nip', $q);
	$this->db->or_like('pangkat', $q);
	$this->db->or_like('golongan', $q);
	$this->db->or_like('status_dosen', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_dosen', $q);
	$this->db->or_like('kode_prodi', $q);
	$this->db->or_like('nama_dosen', $q);
	$this->db->or_like('nip', $q);
	$this->db->or_like('pangkat', $q);
	$this->db->or_like('golongan', $q);
	$this->db->or_like('status_dosen', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Dosen_model.php */
/* Location: ./application/models/Dosen_model.php */
