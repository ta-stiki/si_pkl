
<?php 
/**
 * @var CI_Controller $this
 */
?> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> M_dosen</h3>
		<hr />	 
		<?php echo form_open($action);?>
	    <div class="form-group">
				<?php 
					echo form_label('Kode Prodi');
					echo form_error('kode_prodi');
					echo form_input($kode_prodi);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Nama Dosen');
					echo form_error('nama_dosen');
					echo form_input($nama_dosen);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Nip');
					echo form_error('nip');
					echo form_input($nip);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Pangkat');
					echo form_error('pangkat');
					echo form_input($pangkat);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Golongan');
					echo form_error('golongan');
					echo form_input($golongan);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Status Dosen');
					echo form_error('status_dosen');
					echo form_input($status_dosen);
				?>				
			</div>
	    <?php 
			echo form_input($id_dosen);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('m_dosen','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->

    