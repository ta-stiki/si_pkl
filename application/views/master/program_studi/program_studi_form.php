
<?php
/**
 * @var CI_Controller $this
 */
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> ';
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Program Studi</h3>
		<hr />
		<?php echo form_open($action);?>
	    <div class="form-group">
				<?php
					echo form_label('Kode Prodi');
					echo form_error('kode_prodi');
					echo form_input($kode_prodi);
				?>
			</div>
	    <div class="form-group">
				<?php
					echo form_label('Nama Prodi');
					echo form_error('nama_prodi');
					echo form_input($nama_prodi);
				?>
			</div>
	    <div class="form-group">
				<?php
					echo form_label('Jenjang');
					echo form_error('jenjang');
					echo form_input($jenjang);
				?>
			</div>
	    <?php
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('master/program_studi','Batal',array('class'=>'btn btn-flat btn-default'));
						?>
	<?php echo form_close();?>
		</div>
	 </div>

    </section>
	<!-- /.content -->

