<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="<?php if(isset($meta_description)){ echo ucfirst($meta_description);}?>">
  <meta name="keywords" content="<?php if(isset($meta_keywords)){ echo ucfirst($meta_keywords);}?>">
  <meta name="author" content="Andhika Putra Pratama">
  <link rel="icon" href="<?php if(isset($logos)){ echo base_url($logos); }?>"/>
  <title><?php if(isset($web_name)){ echo ucfirst($web_name); }?> | <?php if(isset($title)){ echo ucfirst($title); }?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('resources');?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('resources');?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('resources');?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('resources');?>/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('resources');?>/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<style>
		.login-header{
			width: 95%;
			background: #ffffff;
			margin: auto 2%;
			padding: 3px;
			display: flex;
		}
		.login-header .logo-header{
			width: 120px;
			margin: 10px;
		}
		.login-header .title-header{
			width: 100%;
			text-align: center;
		}
	</style>
</head>
<body class="hold-transition login-page">
<div class="login-header">
	<img src="<?php echo base_url('resources')?>/dist/img/Logo-unud.png" class="logo-header" alt="User Image">
	<div class="title-header">
		<h1>FAKULTAS PARIWISATA UNIVERSITAS UDAYANA</h1>
		<h2>SISTEM INFORMASI PENGOLAHAN PRAKTEK KERJA LAPANGAN (PKL)</h2>
	</div>
</div>
<div class="login-box">
  <div class="login-logo">
    <b><?php echo lang('login_heading');?></b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><?php echo lang('login_subheading');?></p>
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
    <?php echo form_open("auth/login");?>
      <div class="form-group has-feedback">
        <?php echo lang('login_identity_label', 'identity');?>
		<?php echo form_input($identity);?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?php echo lang('login_password_label', 'password');?>
		<?php echo form_input($password);?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <div class="checkbox icheck">
            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
			<?php echo lang('login_remember_label', 'remember');?>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
			<div class="form-group">
				<?php echo form_submit('submit', lang('login_submit_btn'),array('class'=>'btn btn-flat btn-primary'));?>
				<?php echo form_reset('reset', lang('login_reset_btn'),array('class'=>'btn btn-flat btn-default'));?>
			</div>
        </div>
        <!-- /.col -->
      </div>
    <?php echo form_close();?>
    <!-- /.social-auth-links -->
	<div class="text-right">
		<a href="forgot_password"><?php echo lang('login_forgot_password');?></a><br>
	</div>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('resources');?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('resources');?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url('resources');?>/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
