# SISTEM INFORMASI PENGOLAHAN PRAKTEK KERJA LAPANGAN (PKL)

Ini adalah bantuan untuk membuat aplikasi berbasis web dengan framework codeigniter. 

## Aplikasi ini menggunakan : 
* [Codeigniter](https://www.codeigniter.com/) - Framework versi 3.1.8
* [Ion Auth](https://github.com/benedmunds/CodeIgniter-Ion-Auth) - Library Login
* [Harviacode CRUD Generator](https://bitbucket.org/harviacode/codeigniter-crud-generator/src) - Crud Generator Versi 1.4 yang telah sy modifikasi
* [AdminLTE](https://github.com/almasaeed2010/AdminLTE/releases) - AdminLTE template admin yang telah sedikit dimodifikasi

## Download Url
gundakan link berikut untuk mendownload aplikasi, atau gunakan git clone
[https://gitlab.com/ta-stiki/si_pkl/-/archive/master/si_pkl-master.zip](https://gitlab.com/ta-stiki/si_pkl/-/archive/master/si_pkl-master.zip)

## Cara Pakai :
* letakan di folder htdocs
* rubah nama folder seperti yang anda inginkan dan jangan lupa lakukan pengaturan di application/config untuk mengubah base_url() sesuai dengan nama folder yang telah anda setting sebelumnya
* sesuaikan konfigurasi berikut
    ```php
       //lokasi  application\config\database.php
       $db['default'] = array(
       	~
       	'hostname' => 'localhost',
       	'username' => 'root', //sesuaikan dengan username database, atau biarkan default
       	'password' => '', //sesuaikan dengan password database, atau biarkan default
       	'database' => 'ta_pkl', //sesuaikan dengan nama database yang digunakan
       	~
       );
    ```
* buka web browser dan buka url perintah install database
    ```
    http://localhost/[nama_folder_aplikasi]/install
  ```
* sistem telah siap digunakan


## Catatan
* Pastikan sudah menginstall [Git](https://git-scm.com/downloads) pada komputer
* jika menggunakan sistem operasi windows klik ```update.bat``` untuk mendapatkan update terbaru. Pastikan terhubungan dengan internet saat melakukan update
* jika sudah di update jalankan perintah migrasi kembali untuk mengupdate database jika terdapat perubahan
