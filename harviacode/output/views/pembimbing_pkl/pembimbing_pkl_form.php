
<?php 
/**
 * @var CI_Controller $this
 */
?> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Pembimbing_pkl</h3>
		<hr />	 
		<?php echo form_open($action);?>
	    <div class="form-group">
				<?php 
					echo form_label('Id Mahasiswa');
					echo form_error('id_mahasiswa');
					echo form_input($id_mahasiswa);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Id Dosen');
					echo form_error('id_dosen');
					echo form_input($id_dosen);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Semeter');
					echo form_error('semeter');
					echo form_input($semeter);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Judul Laporan');
					echo form_error('judul_laporan');
					echo form_input($judul_laporan);
				?>				
			</div>
	    <?php 
			echo form_input($kode_bimbingan);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('pembimbing_pkl','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->

    