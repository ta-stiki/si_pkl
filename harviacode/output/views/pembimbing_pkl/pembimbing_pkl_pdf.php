
<?php 
/**
 * @var CI_Controller $this
 */
?>
<!doctype html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Pembimbing_pkl List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Mahasiswa</th>
		<th>Id Dosen</th>
		<th>Semeter</th>
		<th>Judul Laporan</th>
		
            </tr><?php
            foreach ($pembimbing_pkl_data as $pembimbing_pkl)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $pembimbing_pkl->id_mahasiswa ?></td>
		      <td><?php echo $pembimbing_pkl->id_dosen ?></td>
		      <td><?php echo $pembimbing_pkl->semeter ?></td>
		      <td><?php echo $pembimbing_pkl->judul_laporan ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>