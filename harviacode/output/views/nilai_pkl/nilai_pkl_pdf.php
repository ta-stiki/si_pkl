
<?php 
/**
 * @var CI_Controller $this
 */
?>
<!doctype html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Nilai_pkl List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Kode Ujian</th>
		<th>Nilai Relatif</th>
		<th>Nilai Absolut</th>
		
            </tr><?php
            foreach ($nilai_pkl_data as $nilai_pkl)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $nilai_pkl->kode_ujian ?></td>
		      <td><?php echo $nilai_pkl->nilai_relatif ?></td>
		      <td><?php echo $nilai_pkl->nilai_absolut ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>