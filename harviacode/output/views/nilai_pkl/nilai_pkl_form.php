
<?php 
/**
 * @var CI_Controller $this
 */
?> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Nilai_pkl</h3>
		<hr />	 
		<?php echo form_open($action);?>
	    <div class="form-group">
				<?php 
					echo form_label('Kode Ujian');
					echo form_error('kode_ujian');
					echo form_input($kode_ujian);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Nilai Relatif');
					echo form_error('nilai_relatif');
					echo form_input($nilai_relatif);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Nilai Absolut');
					echo form_error('nilai_absolut');
					echo form_input($nilai_absolut);
				?>				
			</div>
	    <?php 
			echo form_input($kode_nilai);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('nilai_pkl','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->

    