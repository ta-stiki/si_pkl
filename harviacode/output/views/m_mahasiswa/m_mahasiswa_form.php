
<?php 
/**
 * @var CI_Controller $this
 */
?> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> M_mahasiswa</h3>
		<hr />	 
		<?php echo form_open($action);?>
	    <div class="form-group">
				<?php 
					echo form_label('Kode Prodi');
					echo form_error('kode_prodi');
					echo form_input($kode_prodi);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Nama Mahasiswa');
					echo form_error('nama_mahasiswa');
					echo form_input($nama_mahasiswa);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Jenis Kelamin');
					echo form_error('jenis_kelamin');
					echo form_input($jenis_kelamin);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Alamat');
					echo form_error('alamat');
					echo form_input($alamat);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('No Telp');
					echo form_error('no_telp');
					echo form_input($no_telp);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Email');
					echo form_error('email');
					echo form_input($email);
				?>				
			</div>
	    <?php 
			echo form_input($id_mahasiswa);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('m_mahasiswa','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->

    