
<?php 
/**
 * @var CI_Controller $this
 */
?>
<!doctype html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>M_mahasiswa List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Kode Prodi</th>
		<th>Nama Mahasiswa</th>
		<th>Jenis Kelamin</th>
		<th>Alamat</th>
		<th>No Telp</th>
		<th>Email</th>
		
            </tr><?php
            foreach ($m_mahasiswa_data as $m_mahasiswa)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $m_mahasiswa->kode_prodi ?></td>
		      <td><?php echo $m_mahasiswa->nama_mahasiswa ?></td>
		      <td><?php echo $m_mahasiswa->jenis_kelamin ?></td>
		      <td><?php echo $m_mahasiswa->alamat ?></td>
		      <td><?php echo $m_mahasiswa->no_telp ?></td>
		      <td><?php echo $m_mahasiswa->email ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>