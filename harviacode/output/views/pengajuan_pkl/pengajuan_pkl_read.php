
<?php 
/**
 * @var CI_Controller $this
 */
?>
   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title">Detail Pengajuan_pkl</h3>
		<hr />
        <table class="table">
	    <tr><td>Id Mahasiswa</td><td><?php echo $id_mahasiswa; ?></td></tr>
	    <tr><td>Semester</td><td><?php echo $semester; ?></td></tr>
	    <tr><td>Tujuan Tempat Pkl</td><td><?php echo $tujuan_tempat_pkl; ?></td></tr>
	    <tr><td>Alamat Tempat Pkl</td><td><?php echo $alamat_tempat_pkl; ?></td></tr>
	    <tr><td>No Telp Tempat Pkl</td><td><?php echo $no_telp_tempat_pkl; ?></td></tr>
	    <tr><td>Lama Pkl</td><td><?php echo $lama_pkl; ?></td></tr>
	    <tr><td>Periode Awal</td><td><?php echo $periode_awal; ?></td></tr>
	    <tr><td>Periode Akhir</td><td><?php echo $periode_akhir; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('pengajuan_pkl') ?>" class="btn btn-flat btn-default">Batal</a></td></tr>
	</table>
        </div>
	 </div>
               
    </section>
	<!-- /.content -->