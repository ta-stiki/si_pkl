
<?php 
/**
 * @var CI_Controller $this
 */
?> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Pengajuan_pkl</h3>
		<hr />	 
		<?php echo form_open($action);?>
	    <div class="form-group">
				<?php 
					echo form_label('Id Mahasiswa');
					echo form_error('id_mahasiswa');
					echo form_input($id_mahasiswa);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Semester');
					echo form_error('semester');
					echo form_input($semester);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Tujuan Tempat Pkl');
					echo form_error('tujuan_tempat_pkl');
					echo form_input($tujuan_tempat_pkl);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Alamat Tempat Pkl');
					echo form_error('alamat_tempat_pkl');
					echo form_input($alamat_tempat_pkl);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('No Telp Tempat Pkl');
					echo form_error('no_telp_tempat_pkl');
					echo form_input($no_telp_tempat_pkl);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Lama Pkl');
					echo form_error('lama_pkl');
					echo form_input($lama_pkl);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Periode Awal');
					echo form_error('periode_awal');
					echo form_input($periode_awal);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Periode Akhir');
					echo form_error('periode_akhir');
					echo form_input($periode_akhir);
				?>				
			</div>
	    <?php 
			echo form_input($kode_pkl);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('pengajuan_pkl','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->

    