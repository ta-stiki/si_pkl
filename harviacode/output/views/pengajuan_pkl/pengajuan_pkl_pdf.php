
<?php 
/**
 * @var CI_Controller $this
 */
?>
<!doctype html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Pengajuan_pkl List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Mahasiswa</th>
		<th>Semester</th>
		<th>Tujuan Tempat Pkl</th>
		<th>Alamat Tempat Pkl</th>
		<th>No Telp Tempat Pkl</th>
		<th>Lama Pkl</th>
		<th>Periode Awal</th>
		<th>Periode Akhir</th>
		
            </tr><?php
            foreach ($pengajuan_pkl_data as $pengajuan_pkl)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $pengajuan_pkl->id_mahasiswa ?></td>
		      <td><?php echo $pengajuan_pkl->semester ?></td>
		      <td><?php echo $pengajuan_pkl->tujuan_tempat_pkl ?></td>
		      <td><?php echo $pengajuan_pkl->alamat_tempat_pkl ?></td>
		      <td><?php echo $pengajuan_pkl->no_telp_tempat_pkl ?></td>
		      <td><?php echo $pengajuan_pkl->lama_pkl ?></td>
		      <td><?php echo $pengajuan_pkl->periode_awal ?></td>
		      <td><?php echo $pengajuan_pkl->periode_akhir ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>