
<?php 
/**
 * @var CI_Controller $this
 */
?> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><?php echo anchor('dashboard','<i class="fa fa-dashboard"></i> Beranda</a>')?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
	<?php if(isset($message)){   
		 echo '<div class="alert alert-warning">  
		   <a href="#" class="close" data-dismiss="alert">&times;</a>  
		   '.$message.'
		 </div> '; 
    }  ?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header">
		 <h3 class="box-title"><?php echo $button ;?> Ujian_pkl</h3>
		<hr />	 
		<?php echo form_open($action);?>
	    <div class="form-group">
				<?php 
					echo form_label('Id Mahasiswa');
					echo form_error('id_mahasiswa');
					echo form_input($id_mahasiswa);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Id Dosen Penguji');
					echo form_error('id_dosen_penguji');
					echo form_input($id_dosen_penguji);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Hari Tanggal');
					echo form_error('hari_tanggal');
					echo form_input($hari_tanggal);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Waktu');
					echo form_error('waktu');
					echo form_input($waktu);
				?>				
			</div>
	    <div class="form-group">
				<?php 
					echo form_label('Tempat Pelaksanaan');
					echo form_error('tempat_pelaksanaan');
					echo form_input($tempat_pelaksanaan);
				?>				
			</div>
	    <?php 
			echo form_input($kode_ujian);
	    	echo form_submit('submit', $button , array('class'=>'btn btn-flat btn-primary'));
	        echo anchor('ujian_pkl','Batal',array('class'=>'btn btn-flat btn-default')); 
						?>
	<?php echo form_close();?>
		</div>
	 </div>
               
    </section>
	<!-- /.content -->

    