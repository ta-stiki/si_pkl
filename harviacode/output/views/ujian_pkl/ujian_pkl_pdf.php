
<?php 
/**
 * @var CI_Controller $this
 */
?>
<!doctype html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Ujian_pkl List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Mahasiswa</th>
		<th>Id Dosen Penguji</th>
		<th>Hari Tanggal</th>
		<th>Waktu</th>
		<th>Tempat Pelaksanaan</th>
		
            </tr><?php
            foreach ($ujian_pkl_data as $ujian_pkl)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $ujian_pkl->id_mahasiswa ?></td>
		      <td><?php echo $ujian_pkl->id_dosen_penguji ?></td>
		      <td><?php echo $ujian_pkl->hari_tanggal ?></td>
		      <td><?php echo $ujian_pkl->waktu ?></td>
		      <td><?php echo $ujian_pkl->tempat_pelaksanaan ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>