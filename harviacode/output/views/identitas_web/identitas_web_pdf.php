
<?php 
/**
 * @var CI_Controller $this
 */
?>
<!doctype html>
<html>
    <head>
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Identitas_web List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama Web</th>
		<th>Meta Deskripsi</th>
		<th>Meta Keyword</th>
		<th>Copyright</th>
		<th>Logo</th>
		
            </tr><?php
            foreach ($identitas_web_data as $identitas_web)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $identitas_web->nama_web ?></td>
		      <td><?php echo $identitas_web->meta_deskripsi ?></td>
		      <td><?php echo $identitas_web->meta_keyword ?></td>
		      <td><?php echo $identitas_web->copyright ?></td>
		      <td><?php echo $identitas_web->logo ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>