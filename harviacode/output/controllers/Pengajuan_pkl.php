<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengajuan_pkl extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Pengajuan_pkl_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));        
				$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['title'] = 'pengajuan_pkl';
			$this->get_Meta();
			
			$this->data['_view']='pengajuan_pkl/pengajuan_pkl_list';
			$this->_render_page('layouts/main',$this->data);
		}
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Pengajuan_pkl_model->json();
    }

    public function read($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->Pengajuan_pkl_model->get_by_id($id);
			if ($row) {
				$this->data['kode_pkl'] = $this->form_validation->set_value('kode_pkl',$row->kode_pkl);
				$this->data['id_mahasiswa'] = $this->form_validation->set_value('id_mahasiswa',$row->id_mahasiswa);
				$this->data['semester'] = $this->form_validation->set_value('semester',$row->semester);
				$this->data['tujuan_tempat_pkl'] = $this->form_validation->set_value('tujuan_tempat_pkl',$row->tujuan_tempat_pkl);
				$this->data['alamat_tempat_pkl'] = $this->form_validation->set_value('alamat_tempat_pkl',$row->alamat_tempat_pkl);
				$this->data['no_telp_tempat_pkl'] = $this->form_validation->set_value('no_telp_tempat_pkl',$row->no_telp_tempat_pkl);
				$this->data['lama_pkl'] = $this->form_validation->set_value('lama_pkl',$row->lama_pkl);
				$this->data['periode_awal'] = $this->form_validation->set_value('periode_awal',$row->periode_awal);
				$this->data['periode_akhir'] = $this->form_validation->set_value('periode_akhir',$row->periode_akhir);
	    
				$this->data['title'] = 'pengajuan_pkl';
				$this->get_Meta();
				$this->data['_view'] = 'pengajuan_pkl/pengajuan_pkl_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('pengajuan_pkl'));
			}
		}
    }

    public function create() 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('pengajuan_pkl/create_action');
		    $this->data['kode_pkl'] = array(
				'name'			=> 'kode_pkl',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_pkl'),
				'class'			=> 'form-control',
			);
		    $this->data['id_mahasiswa'] = array(
				'name'			=> 'id_mahasiswa',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('id_mahasiswa'),
				'class'			=> 'form-control',
			);
		    $this->data['semester'] = array(
				'name'			=> 'semester',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('semester'),
				'class'			=> 'form-control',
			);
		    $this->data['tujuan_tempat_pkl'] = array(
				'name'			=> 'tujuan_tempat_pkl',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('tujuan_tempat_pkl'),
				'class'			=> 'form-control',
			);
		    $this->data['alamat_tempat_pkl'] = array(
				'name'			=> 'alamat_tempat_pkl',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('alamat_tempat_pkl'),
				'class'			=> 'form-control',
			);
		    $this->data['no_telp_tempat_pkl'] = array(
				'name'			=> 'no_telp_tempat_pkl',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('no_telp_tempat_pkl'),
				'class'			=> 'form-control',
			);
		    $this->data['lama_pkl'] = array(
				'name'			=> 'lama_pkl',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('lama_pkl'),
				'class'			=> 'form-control',
			);
		    $this->data['periode_awal'] = array(
				'name'			=> 'periode_awal',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('periode_awal'),
				'class'			=> 'form-control',
			);
		    $this->data['periode_akhir'] = array(
				'name'			=> 'periode_akhir',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('periode_akhir'),
				'class'			=> 'form-control',
			);
	
			$this->data['title'] = 'pengajuan_pkl';
			$this->get_Meta();
			$this->data['_view'] = 'pengajuan_pkl/pengajuan_pkl_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_mahasiswa' 			=> $this->input->post('id_mahasiswa',TRUE),
		'semester' 			=> $this->input->post('semester',TRUE),
		'tujuan_tempat_pkl' 			=> $this->input->post('tujuan_tempat_pkl',TRUE),
		'alamat_tempat_pkl' 			=> $this->input->post('alamat_tempat_pkl',TRUE),
		'no_telp_tempat_pkl' 			=> $this->input->post('no_telp_tempat_pkl',TRUE),
		'lama_pkl' 			=> $this->input->post('lama_pkl',TRUE),
		'periode_awal' 			=> $this->input->post('periode_awal',TRUE),
		'periode_akhir' 			=> $this->input->post('periode_akhir',TRUE),
	    );

            $this->Pengajuan_pkl_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('pengajuan_pkl'));
        }
    }
    
    public function update($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->Pengajuan_pkl_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('pengajuan_pkl/update_action');
			    $this->data['kode_pkl'] = array(
					'name'			=> 'kode_pkl',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_pkl', $row->kode_pkl),
					'class'			=> 'form-control',
				);
			    $this->data['id_mahasiswa'] = array(
					'name'			=> 'id_mahasiswa',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('id_mahasiswa', $row->id_mahasiswa),
					'class'			=> 'form-control',
				);
			    $this->data['semester'] = array(
					'name'			=> 'semester',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('semester', $row->semester),
					'class'			=> 'form-control',
				);
			    $this->data['tujuan_tempat_pkl'] = array(
					'name'			=> 'tujuan_tempat_pkl',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('tujuan_tempat_pkl', $row->tujuan_tempat_pkl),
					'class'			=> 'form-control',
				);
			    $this->data['alamat_tempat_pkl'] = array(
					'name'			=> 'alamat_tempat_pkl',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('alamat_tempat_pkl', $row->alamat_tempat_pkl),
					'class'			=> 'form-control',
				);
			    $this->data['no_telp_tempat_pkl'] = array(
					'name'			=> 'no_telp_tempat_pkl',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('no_telp_tempat_pkl', $row->no_telp_tempat_pkl),
					'class'			=> 'form-control',
				);
			    $this->data['lama_pkl'] = array(
					'name'			=> 'lama_pkl',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('lama_pkl', $row->lama_pkl),
					'class'			=> 'form-control',
				);
			    $this->data['periode_awal'] = array(
					'name'			=> 'periode_awal',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('periode_awal', $row->periode_awal),
					'class'			=> 'form-control',
				);
			    $this->data['periode_akhir'] = array(
					'name'			=> 'periode_akhir',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('periode_akhir', $row->periode_akhir),
					'class'			=> 'form-control',
				);
	   
				$this->data['title'] = 'pengajuan_pkl';
				$this->get_Meta();
				$this->data['_view'] = 'pengajuan_pkl/pengajuan_pkl_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('pengajuan_pkl'));
			}
		}
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_pkl', TRUE));
        } else {
            $data = array(
			'id_mahasiswa' 					=> $this->input->post('id_mahasiswa',TRUE),
			'semester' 					=> $this->input->post('semester',TRUE),
			'tujuan_tempat_pkl' 					=> $this->input->post('tujuan_tempat_pkl',TRUE),
			'alamat_tempat_pkl' 					=> $this->input->post('alamat_tempat_pkl',TRUE),
			'no_telp_tempat_pkl' 					=> $this->input->post('no_telp_tempat_pkl',TRUE),
			'lama_pkl' 					=> $this->input->post('lama_pkl',TRUE),
			'periode_awal' 					=> $this->input->post('periode_awal',TRUE),
			'periode_akhir' 					=> $this->input->post('periode_akhir',TRUE),
	    );

            $this->Pengajuan_pkl_model->update($this->input->post('kode_pkl', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('pengajuan_pkl'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pengajuan_pkl_model->get_by_id($id);

        if ($row) {
            $this->Pengajuan_pkl_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('pengajuan_pkl'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('pengajuan_pkl'));
        }
    }
	
	public function get_Meta(){
		
		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {			
			$this->data['name_web'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
    public function _rules() 
    {
	$this->form_validation->set_rules('id_mahasiswa', 'id mahasiswa', 'trim|required');
	$this->form_validation->set_rules('semester', 'semester', 'trim|required');
	$this->form_validation->set_rules('tujuan_tempat_pkl', 'tujuan tempat pkl', 'trim|required');
	$this->form_validation->set_rules('alamat_tempat_pkl', 'alamat tempat pkl', 'trim|required');
	$this->form_validation->set_rules('no_telp_tempat_pkl', 'no telp tempat pkl', 'trim|required');
	$this->form_validation->set_rules('lama_pkl', 'lama pkl', 'trim|required');
	$this->form_validation->set_rules('periode_awal', 'periode awal', 'trim|required');
	$this->form_validation->set_rules('periode_akhir', 'periode akhir', 'trim|required');

	$this->form_validation->set_rules('kode_pkl', 'kode_pkl', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pengajuan_pkl.xls";
        $judul = "pengajuan_pkl";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Mahasiswa");
	xlsWriteLabel($tablehead, $kolomhead++, "Semester");
	xlsWriteLabel($tablehead, $kolomhead++, "Tujuan Tempat Pkl");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat Tempat Pkl");
	xlsWriteLabel($tablehead, $kolomhead++, "No Telp Tempat Pkl");
	xlsWriteLabel($tablehead, $kolomhead++, "Lama Pkl");
	xlsWriteLabel($tablehead, $kolomhead++, "Periode Awal");
	xlsWriteLabel($tablehead, $kolomhead++, "Periode Akhir");

	foreach ($this->Pengajuan_pkl_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_mahasiswa);
	    xlsWriteLabel($tablebody, $kolombody++, $data->semester);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tujuan_tempat_pkl);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat_tempat_pkl);
	    xlsWriteLabel($tablebody, $kolombody++, $data->no_telp_tempat_pkl);
	    xlsWriteLabel($tablebody, $kolombody++, $data->lama_pkl);
	    xlsWriteLabel($tablebody, $kolombody++, $data->periode_awal);
	    xlsWriteLabel($tablebody, $kolombody++, $data->periode_akhir);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdf()
    {
        $data = array(
            'pengajuan_pkl_data' => $this->Pengajuan_pkl_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('pengajuan_pkl/pengajuan_pkl_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('pengajuan_pkl.pdf', 'D'); 
    }

}

/* End of file Pengajuan_pkl.php */
/* Location: ./application/controllers/Pengajuan_pkl.php */
