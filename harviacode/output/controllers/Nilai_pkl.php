<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nilai_pkl extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('Nilai_pkl_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));        
				$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['title'] = 'nilai_pkl';
			$this->get_Meta();
			
			$this->data['_view']='nilai_pkl/nilai_pkl_list';
			$this->_render_page('layouts/main',$this->data);
		}
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Nilai_pkl_model->json();
    }

    public function read($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->Nilai_pkl_model->get_by_id($id);
			if ($row) {
				$this->data['kode_nilai'] = $this->form_validation->set_value('kode_nilai',$row->kode_nilai);
				$this->data['kode_ujian'] = $this->form_validation->set_value('kode_ujian',$row->kode_ujian);
				$this->data['nilai_relatif'] = $this->form_validation->set_value('nilai_relatif',$row->nilai_relatif);
				$this->data['nilai_absolut'] = $this->form_validation->set_value('nilai_absolut',$row->nilai_absolut);
	    
				$this->data['title'] = 'nilai_pkl';
				$this->get_Meta();
				$this->data['_view'] = 'nilai_pkl/nilai_pkl_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('nilai_pkl'));
			}
		}
    }

    public function create() 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('nilai_pkl/create_action');
		    $this->data['kode_nilai'] = array(
				'name'			=> 'kode_nilai',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_nilai'),
				'class'			=> 'form-control',
			);
		    $this->data['kode_ujian'] = array(
				'name'			=> 'kode_ujian',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_ujian'),
				'class'			=> 'form-control',
			);
		    $this->data['nilai_relatif'] = array(
				'name'			=> 'nilai_relatif',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nilai_relatif'),
				'class'			=> 'form-control',
			);
		    $this->data['nilai_absolut'] = array(
				'name'			=> 'nilai_absolut',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nilai_absolut'),
				'class'			=> 'form-control',
			);
	
			$this->data['title'] = 'nilai_pkl';
			$this->get_Meta();
			$this->data['_view'] = 'nilai_pkl/nilai_pkl_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kode_ujian' 			=> $this->input->post('kode_ujian',TRUE),
		'nilai_relatif' 			=> $this->input->post('nilai_relatif',TRUE),
		'nilai_absolut' 			=> $this->input->post('nilai_absolut',TRUE),
	    );

            $this->Nilai_pkl_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('nilai_pkl'));
        }
    }
    
    public function update($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->Nilai_pkl_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('nilai_pkl/update_action');
			    $this->data['kode_nilai'] = array(
					'name'			=> 'kode_nilai',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_nilai', $row->kode_nilai),
					'class'			=> 'form-control',
				);
			    $this->data['kode_ujian'] = array(
					'name'			=> 'kode_ujian',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_ujian', $row->kode_ujian),
					'class'			=> 'form-control',
				);
			    $this->data['nilai_relatif'] = array(
					'name'			=> 'nilai_relatif',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('nilai_relatif', $row->nilai_relatif),
					'class'			=> 'form-control',
				);
			    $this->data['nilai_absolut'] = array(
					'name'			=> 'nilai_absolut',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('nilai_absolut', $row->nilai_absolut),
					'class'			=> 'form-control',
				);
	   
				$this->data['title'] = 'nilai_pkl';
				$this->get_Meta();
				$this->data['_view'] = 'nilai_pkl/nilai_pkl_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('nilai_pkl'));
			}
		}
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_nilai', TRUE));
        } else {
            $data = array(
			'kode_ujian' 					=> $this->input->post('kode_ujian',TRUE),
			'nilai_relatif' 					=> $this->input->post('nilai_relatif',TRUE),
			'nilai_absolut' 					=> $this->input->post('nilai_absolut',TRUE),
	    );

            $this->Nilai_pkl_model->update($this->input->post('kode_nilai', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('nilai_pkl'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Nilai_pkl_model->get_by_id($id);

        if ($row) {
            $this->Nilai_pkl_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('nilai_pkl'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('nilai_pkl'));
        }
    }
	
	public function get_Meta(){
		
		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {			
			$this->data['name_web'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
    public function _rules() 
    {
	$this->form_validation->set_rules('kode_ujian', 'kode ujian', 'trim|required');
	$this->form_validation->set_rules('nilai_relatif', 'nilai relatif', 'trim|required');
	$this->form_validation->set_rules('nilai_absolut', 'nilai absolut', 'trim|required');

	$this->form_validation->set_rules('kode_nilai', 'kode_nilai', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "nilai_pkl.xls";
        $judul = "nilai_pkl";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode Ujian");
	xlsWriteLabel($tablehead, $kolomhead++, "Nilai Relatif");
	xlsWriteLabel($tablehead, $kolomhead++, "Nilai Absolut");

	foreach ($this->Nilai_pkl_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kode_ujian);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nilai_relatif);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nilai_absolut);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    function pdf()
    {
        $data = array(
            'nilai_pkl_data' => $this->Nilai_pkl_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('nilai_pkl/nilai_pkl_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('nilai_pkl.pdf', 'D'); 
    }

}

/* End of file Nilai_pkl.php */
/* Location: ./application/controllers/Nilai_pkl.php */
