<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembimbing_pkl_model extends CI_Model
{

    public $table = 'pembimbing_pkl';
    public $id = 'kode_bimbingan';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('kode_bimbingan,id_mahasiswa,id_dosen,semeter,judul_laporan');
        $this->datatables->from('pembimbing_pkl');
        //add this line for join
        //$this->datatables->join('table2', 'pembimbing_pkl.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('pembimbing_pkl/read/$1'),'<i class = "fa fa-eye"></i>', array('class'=>'btn btn-flat btn-info'))." ".anchor(site_url('pembimbing_pkl/update/$1'),'<i class = "fa fa-edit"></i>', array('class'=>'btn btn-flat btn-warning'))." ".anchor(site_url('pembimbing_pkl/delete/$1'),'<i class = "fa fa-trash"></i>', array('class'=>'btn btn-flat btn-danger'),'onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'kode_bimbingan');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('kode_bimbingan', $q);
	$this->db->or_like('id_mahasiswa', $q);
	$this->db->or_like('id_dosen', $q);
	$this->db->or_like('semeter', $q);
	$this->db->or_like('judul_laporan', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('kode_bimbingan', $q);
	$this->db->or_like('id_mahasiswa', $q);
	$this->db->or_like('id_dosen', $q);
	$this->db->or_like('semeter', $q);
	$this->db->or_like('judul_laporan', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Pembimbing_pkl_model.php */
/* Location: ./application/models/Pembimbing_pkl_model.php */
