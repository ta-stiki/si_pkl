<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengajuan_pkl_model extends CI_Model
{

    public $table = 'pengajuan_pkl';
    public $id = 'kode_pkl';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('kode_pkl,id_mahasiswa,semester,tujuan_tempat_pkl,alamat_tempat_pkl,no_telp_tempat_pkl,lama_pkl,periode_awal,periode_akhir');
        $this->datatables->from('pengajuan_pkl');
        //add this line for join
        //$this->datatables->join('table2', 'pengajuan_pkl.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('pengajuan_pkl/read/$1'),'<i class = "fa fa-eye"></i>', array('class'=>'btn btn-flat btn-info'))." ".anchor(site_url('pengajuan_pkl/update/$1'),'<i class = "fa fa-edit"></i>', array('class'=>'btn btn-flat btn-warning'))." ".anchor(site_url('pengajuan_pkl/delete/$1'),'<i class = "fa fa-trash"></i>', array('class'=>'btn btn-flat btn-danger'),'onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'kode_pkl');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('kode_pkl', $q);
	$this->db->or_like('id_mahasiswa', $q);
	$this->db->or_like('semester', $q);
	$this->db->or_like('tujuan_tempat_pkl', $q);
	$this->db->or_like('alamat_tempat_pkl', $q);
	$this->db->or_like('no_telp_tempat_pkl', $q);
	$this->db->or_like('lama_pkl', $q);
	$this->db->or_like('periode_awal', $q);
	$this->db->or_like('periode_akhir', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('kode_pkl', $q);
	$this->db->or_like('id_mahasiswa', $q);
	$this->db->or_like('semester', $q);
	$this->db->or_like('tujuan_tempat_pkl', $q);
	$this->db->or_like('alamat_tempat_pkl', $q);
	$this->db->or_like('no_telp_tempat_pkl', $q);
	$this->db->or_like('lama_pkl', $q);
	$this->db->or_like('periode_awal', $q);
	$this->db->or_like('periode_akhir', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Pengajuan_pkl_model.php */
/* Location: ./application/models/Pengajuan_pkl_model.php */
