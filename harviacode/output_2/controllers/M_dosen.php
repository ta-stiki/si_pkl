<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_dosen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->database();
        $this->load->model(array('M_dosen_model','Identitas_web_model'));
        $this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url', 'html'));        
				$this->load->library('datatables');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['title'] = 'm_dosen';
			$this->get_Meta();
			
			$this->data['_view']='m_dosen/m_dosen_list';
			$this->_render_page('layouts/main',$this->data);
		}
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->M_dosen_model->json();
    }

    public function read($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->M_dosen_model->get_by_id($id);
			if ($row) {
				$this->data['id_dosen'] = $this->form_validation->set_value('id_dosen',$row->id_dosen);
				$this->data['kode_prodi'] = $this->form_validation->set_value('kode_prodi',$row->kode_prodi);
				$this->data['nama_dosen'] = $this->form_validation->set_value('nama_dosen',$row->nama_dosen);
				$this->data['nip'] = $this->form_validation->set_value('nip',$row->nip);
				$this->data['pangkat'] = $this->form_validation->set_value('pangkat',$row->pangkat);
				$this->data['golongan'] = $this->form_validation->set_value('golongan',$row->golongan);
				$this->data['status_dosen'] = $this->form_validation->set_value('status_dosen',$row->status_dosen);
	    
				$this->data['title'] = 'm_dosen';
				$this->get_Meta();
				$this->data['_view'] = 'm_dosen/m_dosen_read';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data tidak ditemukan';
				redirect(site_url('m_dosen'));
			}
		}
    }

    public function create() 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$this->data['button'] = 'Tambah';
			$this->data['action'] = site_url('m_dosen/create_action');
		    $this->data['id_dosen'] = array(
				'name'			=> 'id_dosen',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('id_dosen'),
				'class'			=> 'form-control',
			);
		    $this->data['kode_prodi'] = array(
				'name'			=> 'kode_prodi',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('kode_prodi'),
				'class'			=> 'form-control',
			);
		    $this->data['nama_dosen'] = array(
				'name'			=> 'nama_dosen',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nama_dosen'),
				'class'			=> 'form-control',
			);
		    $this->data['nip'] = array(
				'name'			=> 'nip',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('nip'),
				'class'			=> 'form-control',
			);
		    $this->data['pangkat'] = array(
				'name'			=> 'pangkat',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('pangkat'),
				'class'			=> 'form-control',
			);
		    $this->data['golongan'] = array(
				'name'			=> 'golongan',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('golongan'),
				'class'			=> 'form-control',
			);
		    $this->data['status_dosen'] = array(
				'name'			=> 'status_dosen',
				'type'			=> 'text',
				'value'			=> $this->form_validation->set_value('status_dosen'),
				'class'			=> 'form-control',
			);
	
			$this->data['title'] = 'm_dosen';
			$this->get_Meta();
			$this->data['_view'] = 'm_dosen/m_dosen_form';
			$this->_render_page('layouts/main',$this->data);
		}
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kode_prodi' 			=> $this->input->post('kode_prodi',TRUE),
		'nama_dosen' 			=> $this->input->post('nama_dosen',TRUE),
		'nip' 			=> $this->input->post('nip',TRUE),
		'pangkat' 			=> $this->input->post('pangkat',TRUE),
		'golongan' 			=> $this->input->post('golongan',TRUE),
		'status_dosen' 			=> $this->input->post('status_dosen',TRUE),
	    );

            $this->M_dosen_model->insert($data);
            $this->data['message'] = 'Data berhasil ditambahkan';
            redirect(site_url('m_dosen'));
        }
    }
    
    public function update($id) 
    {
        if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('Anda tidak punya akses di halaman ini');
		}
		else
		{
			$this->data['user'] = $this->ion_auth->user()->row();
			
			$row = $this->M_dosen_model->get_by_id($id);

			if ($row) {
				$this->data['button']		= 'Ubah';
				$this->data['action']		= site_url('m_dosen/update_action');
			    $this->data['id_dosen'] = array(
					'name'			=> 'id_dosen',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('id_dosen', $row->id_dosen),
					'class'			=> 'form-control',
				);
			    $this->data['kode_prodi'] = array(
					'name'			=> 'kode_prodi',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('kode_prodi', $row->kode_prodi),
					'class'			=> 'form-control',
				);
			    $this->data['nama_dosen'] = array(
					'name'			=> 'nama_dosen',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('nama_dosen', $row->nama_dosen),
					'class'			=> 'form-control',
				);
			    $this->data['nip'] = array(
					'name'			=> 'nip',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('nip', $row->nip),
					'class'			=> 'form-control',
				);
			    $this->data['pangkat'] = array(
					'name'			=> 'pangkat',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('pangkat', $row->pangkat),
					'class'			=> 'form-control',
				);
			    $this->data['golongan'] = array(
					'name'			=> 'golongan',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('golongan', $row->golongan),
					'class'			=> 'form-control',
				);
			    $this->data['status_dosen'] = array(
					'name'			=> 'status_dosen',
					'type'			=> 'text',
					'value'			=> $this->form_validation->set_value('status_dosen', $row->status_dosen),
					'class'			=> 'form-control',
				);
	   
				$this->data['title'] = 'm_dosen';
				$this->get_Meta();
				$this->data['_view'] = 'm_dosen/m_dosen_form';
				$this->_render_page('layouts/main',$this->data);
			} else {
				$this->data['message'] = 'Data Tidak Ditemukan';
				redirect(site_url('m_dosen'));
			}
		}
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_dosen', TRUE));
        } else {
            $data = array(
			'kode_prodi' 					=> $this->input->post('kode_prodi',TRUE),
			'nama_dosen' 					=> $this->input->post('nama_dosen',TRUE),
			'nip' 					=> $this->input->post('nip',TRUE),
			'pangkat' 					=> $this->input->post('pangkat',TRUE),
			'golongan' 					=> $this->input->post('golongan',TRUE),
			'status_dosen' 					=> $this->input->post('status_dosen',TRUE),
	    );

            $this->M_dosen_model->update($this->input->post('id_dosen', TRUE), $data);
            $this->data['message'] = 'Data berhasil di ubah';
            redirect(site_url('m_dosen'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->M_dosen_model->get_by_id($id);

        if ($row) {
            $this->M_dosen_model->delete($id);
            $this->data['message'] = 'Hapus data berhasil';
            redirect(site_url('m_dosen'));
        } else {
            $this->data['message'] = 'Data tidak ditemukan';
            redirect(site_url('m_dosen'));
        }
    }
	
	public function get_Meta(){
		
		$rows = $this->Identitas_web_model->get_all();
		foreach ($rows as $row) {			
			$this->data['name_web'] 		= $this->form_validation->set_value('nama_web',$row->nama_web);
			$this->data['meta_description']= $this->form_validation->set_value('meta_deskripsi',$row->meta_deskripsi);
			$this->data['meta_keywords'] 	= $this->form_validation->set_value('meta_keyword',$row->meta_keyword);
			$this->data['copyrights'] 		= $this->form_validation->set_value('copyright',$row->copyright);
			$this->data['logos'] 		= $this->form_validation->set_value('logo',$row->logo);
	    }
	}
	
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}
	
    public function _rules() 
    {
	$this->form_validation->set_rules('kode_prodi', 'kode prodi', 'trim|required');
	$this->form_validation->set_rules('nama_dosen', 'nama dosen', 'trim|required');
	$this->form_validation->set_rules('nip', 'nip', 'trim|required');
	$this->form_validation->set_rules('pangkat', 'pangkat', 'trim|required');
	$this->form_validation->set_rules('golongan', 'golongan', 'trim|required');
	$this->form_validation->set_rules('status_dosen', 'status dosen', 'trim|required');

	$this->form_validation->set_rules('id_dosen', 'id_dosen', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file M_dosen.php */
/* Location: ./application/controllers/M_dosen.php */
