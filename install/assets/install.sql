/*
 Navicat Premium Data Transfer

 Source Server         : Local MariaDb
 Source Server Type    : MariaDB
 Source Server Version : 100412
 Source Host           : localhost:3306
 Source Schema         : ta_pkl

 Target Server Type    : MariaDB
 Target Server Version : 100412
 File Encoding         : 65001

 Date: 29/08/2020 15:10:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'koordinator_pkl', 'Koordinator PKL');
INSERT INTO `groups` VALUES (3, 'prodi', 'Staff Prodi');
INSERT INTO `groups` VALUES (4, 'siswa', 'Siswa');

-- ----------------------------
-- Table structure for identitas_web
-- ----------------------------
DROP TABLE IF EXISTS `identitas_web`;
CREATE TABLE `identitas_web`  (
  `id_identitas` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_web` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `meta_deskripsi` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `copyright` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_identitas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of identitas_web
-- ----------------------------
INSERT INTO `identitas_web` VALUES (1, 'SIMPLA', 'asss', 'asdfss', 'dx', './uploads/1b8631d07e2d37a436894d16eb365f7c.png');

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for m_dosen
-- ----------------------------
DROP TABLE IF EXISTS `m_dosen`;
CREATE TABLE `m_dosen`  (
  `id_dosen` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kode_prodi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_dosen` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pangkat` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `golongan` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status_dosen` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_dosen`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_dosen
-- ----------------------------

-- ----------------------------
-- Table structure for m_mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `m_mahasiswa`;
CREATE TABLE `m_mahasiswa`  (
  `id_mahasiswa` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kode_prodi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_mahasiswa` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jenis_kelamin` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alamat` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `no_telp` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_mahasiswa`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_mahasiswa
-- ----------------------------

-- ----------------------------
-- Table structure for m_periode
-- ----------------------------
DROP TABLE IF EXISTS `m_periode`;
CREATE TABLE `m_periode`  (
  `kode_periode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama_periode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tahun_akademik` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kode_periode`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_periode
-- ----------------------------

-- ----------------------------
-- Table structure for m_program_studi
-- ----------------------------
DROP TABLE IF EXISTS `m_program_studi`;
CREATE TABLE `m_program_studi`  (
  `kode_prodi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama_prodi` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jenjang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kode_prodi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_program_studi
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_menu` int(11) NULL DEFAULT NULL,
  `nama_menu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `controller_link` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `urut_menu` int(11) NOT NULL,
  `menu_grup_user` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 0, 'Master', '#', 'fa-list-alt', 'a', 1, '1', 1);
INSERT INTO `menu` VALUES (2, 1, 'Data Periode', 'master/periode', 'fa-calendar', 'periode', 1, '1', 1);
INSERT INTO `menu` VALUES (3, 1, 'Data Dosen', 'master/dosen', 'fa-user-secret', 'dosen', 2, '2', 1);
INSERT INTO `menu` VALUES (4, 1, 'Data Mahasiswa', 'master/siswa', 'fa-users', 'siswa', 3, '2', 1);
INSERT INTO `menu` VALUES (5, 1, 'Data Program Sudi', 'master/program_studi', 'fa-graduation-cap', 'prodi', 4, '3', 1);
INSERT INTO `menu` VALUES (6, 0, 'PKL', '#', 'fa-address-book-o', 'pkl', 2, '3', 1);
INSERT INTO `menu` VALUES (7, 6, 'Data Pengajuan PKL', 'pkl/pengajuan', 'fa-address-book', 'pengajuan-pkl', 2, '1', 1);
INSERT INTO `menu` VALUES (8, 6, 'Data Pembimbing PKL', 'pkl/pembimbing', 'fa-id-card', 'pembimbing-pkl', 1, '1', 1);
INSERT INTO `menu` VALUES (9, 6, 'Data Ujian PKL', 'pkl/ujian', 'fa-tasks', 'ujian-pkl', 3, '1', 1);
INSERT INTO `menu` VALUES (10, 6, 'Data Nilai PKL', 'pkl/nilai', 'fa-file-text', 'nilai-pkl', 4, '1', 1);
INSERT INTO `menu` VALUES (11, 0, 'Laporan', '#', 'fa-newspaper-o', 'laporan', 3, '1', 1);
INSERT INTO `menu` VALUES (12, 11, 'Laporan Pengajuan PKL', 'laporan/pengajuan-pkl', 'fa-newspaper-o', 'lap-pengajuan-pkl', 1, '1', 1);
INSERT INTO `menu` VALUES (13, 11, 'Laporan Pembimbing Penguji', 'laporan/pembimbing', 'fa-newspaper-o', 'lap-pembimbing-penguji', 2, '1', 1);
INSERT INTO `menu` VALUES (14, 11, 'Laporan Nilai PKL', 'laporan/nilai_pkl', 'fa-newspaper-o', 'lap-nillai-pkl', 3, '1', 1);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `version` bigint(20) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (4);

-- ----------------------------
-- Table structure for nilai_pkl
-- ----------------------------
DROP TABLE IF EXISTS `nilai_pkl`;
CREATE TABLE `nilai_pkl`  (
  `kode_nilai` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kode_ujian` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nilai_relatif` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nilai_absolut` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kode_nilai`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nilai_pkl
-- ----------------------------

-- ----------------------------
-- Table structure for pembimbing_pkl
-- ----------------------------
DROP TABLE IF EXISTS `pembimbing_pkl`;
CREATE TABLE `pembimbing_pkl`  (
  `kode_bimbingan` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_mahasiswa` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_dosen` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `semeter` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `judul_laporan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kode_bimbingan`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pembimbing_pkl
-- ----------------------------

-- ----------------------------
-- Table structure for pengajuan_pkl
-- ----------------------------
DROP TABLE IF EXISTS `pengajuan_pkl`;
CREATE TABLE `pengajuan_pkl`  (
  `kode_pkl` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_mahasiswa` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `semester` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tujuan_tempat_pkl` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alamat_tempat_pkl` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `no_telp_tempat_pkl` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lama_pkl` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `periode_awal` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `periode_akhir` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kode_pkl`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengajuan_pkl
-- ----------------------------

-- ----------------------------
-- Table structure for ujian_pkl
-- ----------------------------
DROP TABLE IF EXISTS `ujian_pkl`;
CREATE TABLE `ujian_pkl`  (
  `kode_ujian` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_mahasiswa` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_dosen_penguji` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hari_tanggal` date NULL DEFAULT NULL,
  `waktu` time(0) NULL DEFAULT NULL,
  `tempat_pelaksanaan` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kode_ujian`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ujian_pkl
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salt` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `remember_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_category` enum('administrator','siswa','prodi','koordinator','') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1268889823, 1, 'Admin', 'istrator', 'ADMIN', '0', 'administrator', NULL);

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (1, 1, 1);
INSERT INTO `users_groups` VALUES (2, 1, 2);
INSERT INTO `users_groups` VALUES (3, 1, 3);
INSERT INTO `users_groups` VALUES (4, 1, 4);

SET FOREIGN_KEY_CHECKS = 1;
